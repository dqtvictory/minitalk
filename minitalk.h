/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minitalk.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/01 22:40:59 by qdam              #+#    #+#             */
/*   Updated: 2021/06/06 18:11:28 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINITALK_H
# define MINITALK_H

# include <signal.h>
# include <stdbool.h>
# include "libft/libft.h"

# define ARG_ERROR "Must have server's pid and a message as argument"
# define PID_ERROR "Invalid server's pid or server cannot receive signal"
# define ACKN_RECV "Server received the message"
# define ACKN_DONE "Server has done displaying the message"

# define LISTENING "Server listening on pid "

# define B0 SIGUSR1
# define B1 SIGUSR2

typedef struct s_byte
{
	int		client;
	char	c;
	int		mask;
}	t_byte;

void	init_mask(void);
void	send_char(int pid, char c);

#endif
