/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/01 22:41:20 by qdam              #+#    #+#             */
/*   Updated: 2021/06/06 18:06:31 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

static t_byte	g_byte;

static void	bit0_hdl(int sig, siginfo_t *info, void *f)
{
	(void)sig;
	(void)f;
	if (!g_byte.client)
	{
		g_byte.client = info->si_pid;
		kill(g_byte.client, SIGUSR1);
	}
	if (g_byte.client == info->si_pid)
		g_byte.mask >>= 1;
}

static void	bit1_hdl(int sig, siginfo_t *info, void *f)
{
	(void)sig;
	(void)f;
	if (!g_byte.client)
	{
		g_byte.client = info->si_pid;
		kill(g_byte.client, SIGUSR1);
	}
	if (g_byte.client == info->si_pid)
	{
		g_byte.c |= g_byte.mask;
		g_byte.mask >>= 1;
	}
}

static void	init_sigaction(struct sigaction *sa)
{
	ft_bzero(sa, sizeof(struct sigaction) * 2);
	sa[0].sa_flags = SA_SIGINFO;
	sa[0].sa_sigaction = &bit0_hdl;
	sigaction(B0, &sa[0], NULL);
	sa[1].sa_flags = SA_SIGINFO;
	sa[1].sa_sigaction = &bit1_hdl;
	sigaction(B1, &sa[1], NULL);
}

int	main(void)
{
	struct sigaction	sa[2];

	write(STDOUT_FILENO, LISTENING, ft_strlen(LISTENING));
	ft_putnbr_fd(getpid(), STDOUT_FILENO);
	init_sigaction(sa);
	while (1)
	{
		if (!g_byte.mask)
		{
			if (!g_byte.c)
			{
				write(STDOUT_FILENO, "\n", 1);
				if (g_byte.client)
					kill(g_byte.client, SIGUSR2);
				g_byte.client = 0;
			}
			else
				write(STDOUT_FILENO, &g_byte.c, 1);
			g_byte.c = 0;
			g_byte.mask = 128;
		}
		pause();
	}
}
