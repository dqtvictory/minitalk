INC_DIR		= .

CLIENT_SRCS	= client.c client_sender.c
SERVER_SRCS	= server.c

LIBFT_DIR	= libft

CLIENT_OBJS	= ${CLIENT_SRCS:.c=.o}
SERVER_OBJS	= ${SERVER_SRCS:.c=.o}

NAME		= minitalk

CLIENT_NAME	= client
SERVER_NAME	= server

CC			= gcc
CFLAGS		= -Wall -Wextra -Werror

RM			= rm -f

.c.o:
				${CC} ${CFLAGS} -I ${INC_DIR} -c $< -o ${<:.c=.o}

${NAME}:		${CLIENT_NAME} ${SERVER_NAME}

${CLIENT_NAME}:	${CLIENT_OBJS}
				make -C ${LIBFT_DIR}
				${CC} ${CFLAGS} -o ${CLIENT_NAME} ${CLIENT_OBJS} -L${LIBFT_DIR} -lft

${SERVER_NAME}:	${SERVER_OBJS}
				make -C ${LIBFT_DIR}
				${CC} ${CFLAGS} -o ${SERVER_NAME} ${SERVER_OBJS} -L${LIBFT_DIR} -lft

all:			${NAME}

clean:
				${RM} ${CLIENT_OBJS} ${SERVER_OBJS}
				make clean -C ${LIBFT_DIR}

fclean:			clean
				make fclean -C ${LIBFT_DIR}
				${RM} ${CLIENT_NAME} ${SERVER_NAME}

re:				fclean all

.PHONY:			all clean fclean re
