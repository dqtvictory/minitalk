/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: qdam <qdam@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/01 23:17:58 by qdam              #+#    #+#             */
/*   Updated: 2021/06/06 12:22:32 by qdam             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

static int	print(int fd, char *msg)
{
	write(fd, msg, ft_strlen(msg));
	write(fd, "\n", 1);
	return (1);
}

static void	sig1_hdl(int sig)
{
	(void)sig;
	print(STDOUT_FILENO, ACKN_RECV);
}

static void	sig2_hdl(int sig)
{
	(void)sig;
	print(STDOUT_FILENO, ACKN_DONE);
	exit(0);
}

int	main(int ac, char **av)
{
	int		s_id;
	char	*msg;

	if (ac != 3)
		return (print(STDERR_FILENO, ARG_ERROR));
	s_id = ft_atoi(av[1]);
	if (!s_id || kill(s_id, 0))
		return (print(STDERR_FILENO, PID_ERROR));
	signal(SIGUSR1, sig1_hdl);
	signal(SIGUSR2, sig2_hdl);
	msg = av[2];
	init_mask();
	while (*msg)
	{
		send_char(s_id, *msg++);
		usleep(50);
	}
	send_char(s_id, 0);
	while (1)
		pause();
}
