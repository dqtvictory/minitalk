/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_sender.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dqtvictory <dqtvictory@student.42.fr>      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/06/05 02:12:29 by dqtvictory        #+#    #+#             */
/*   Updated: 2021/06/06 18:07:48 by dqtvictory       ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

static int	g_mask[8];

void	init_mask(void)
{
	int	i;
	int	m;

	i = 7;
	m = 1;
	while (i >= 0)
	{
		g_mask[i] = m;
		i--;
		m <<= 1;
	}
}

void	send_char(int pid, char c)
{
	int		i;
	char	b;

	i = 0;
	while (i < 8)
	{
		b = c & g_mask[i];
		kill(pid, B0 * (b == 0) + B1 * (b != 0));
		usleep(30);
		i++;
	}
}
